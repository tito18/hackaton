pragma solidity ^0.4.25;

import "github.com/Arachnid/solidity-stringutils/strings.sol";

contract Id_Online {
    
    using strings for *;
    
    string  public  birthdate;
    address public  Gobierno = 0xca35b7d915458ef540ade6068dfe2f44e8fa733c; 
    string  public  alerta = "Su hijo esta mueriendo";
    string  public  mensaje = "Cita ";
    
    
    struct Instituciones{
        string nombre;
        string cel;
        string email;
        string dir;
    }
    mapping(address => Instituciones)instituto;
    address[] public institutoAccts;
    
    struct Enfermedad{
        string nombre;
        string tratamiento;
    }
    mapping(address => Enfermedad)enfermedades;
    address[] public enfermedadAccts;
    
    
    struct Alergia{
        string nombre;
        string razon; //que las provocas
    }
    mapping(uint => Alergia) alergias;
    uint[] public alergiasAccts;
    
    
    struct Cita{
        address doctor;
        address paciente;
        uint tipo;
        string motivo;
        string day;
        string month;
        string year;

    }
    mapping(uint => Cita) citas;
    uint[] public citaAccts;
    
    struct Person{
        string id;
        string fName;
        string lName;
        string gender;
        string blood;
        uint age;
        string day;
        string month;
        string year;
        address dad;
        address mom;
        address[] son;

        string profesion;
        string cel;
        uint[] alergias;
        string dir;
        string email;
        address[] enfermedades;
        uint[] citasM;
    }
       
    mapping (address => Person) persons;
    address[] public personAccts;

    constructor () public {
    }
    
    //FUNCIONES

    modifier restrigeresul(){
        require(msg.sender == Gobierno);
        _;
    }

    function setInstituto(address _address, string _nombre, string _cel, string _email, string _dir) restrigeresul public{
        instituto[_address].nombre  = _nombre;
        instituto[_address].cel     = _cel;
        instituto[_address].email   = _email;
        instituto[_address].dir     = _dir;

        institutoAccts.push(_address) -1;
    }

    function setEnfermedad(address _address, string _nombre, string _tratamiento)restrigeresul public{
        enfermedades[_address].nombre         = _nombre;
        enfermedades[_address].tratamiento    = _tratamiento;
        
        enfermedadAccts.push(_address) -1;
    }
    
    function setAlergias(uint _addressAle, string _nombre, string _razon)restrigeresul public{
        alergias[_addressAle].nombre    = _nombre;
        alergias[_addressAle].razon     = _razon;
        
        alergiasAccts.push(_addressAle) -1;
    }
     // porque se le llama enfermo?
    function declararPersonAlergia(address _enfermo, uint _alergia)restrigeresul public{
        persons[_enfermo].alergias.push(_alergia);
    }
    
    function declararPersonEnfermedad(address _enfermo, address _enfermedad)restrigeresul public{
        persons[_enfermo].enfermedades.push(_enfermedad);
    }
    
    function modifyCelDirEmail(address _address, string _cel, string _email, string _dir)restrigeresul public{
        persons[_address].cel    = _cel;
        persons[_address].email  = _email;
        persons[_address].dir    = _dir;
    }
    
    function setCita(uint _cita, address _doctor, address _paciente, uint _tipo, string _motivo,
    string _day, string _month, string _year)restrigeresul public returns(string){
        
        citas[_cita].doctor    = _doctor;
        citas[_cita].paciente  = _paciente;
        citas[_cita].tipo      = _tipo;
        citas[_cita].motivo    = _motivo;
        citas[_cita].day       = _day;
        citas[_cita].month     = _month;
        citas[_cita].year      = _year;
        
        persons[_paciente].citasM.push(_cita);
        citaAccts.push(_cita) -1;

        //_tipo = "urgencia"
        if (_tipo == 3){
            
            mensaje = mensaje.toSlice().concat(alerta.toSlice());
            mensaje = mensaje.toSlice().concat(persons[persons[_paciente].dad].cel.toSlice());
            mensaje = mensaje.toSlice().concat(persons[persons[_paciente].mom].cel.toSlice());     
                
        }
        else
        
        return mensaje;
    }
    
    function setPersonGrown(address _address, string _id, uint _age, string _fName, string _lName,
    string _gender, string _blood, string _day, string _month, string _year, string _cel, string _dir
    , string _email)restrigeresul public{
                
        persons[_address].id      = _id;
        persons[_address].blood   = _blood;
        persons[_address].gender  = _gender;
        persons[_address].age     = _age;
        persons[_address].fName   = _fName;
        persons[_address].lName   = _lName;
        persons[_address].day     = _day;
        persons[_address].month   = _month;
        persons[_address].year    = _year;
        persons[_address].cel     = _cel;
        persons[_address].email   = _email;
        persons[_address].dir     = _dir;
                
        personAccts.push(_address) -1;
    }

    function setPersonBorn(address _address, string _id, uint _age, string _fName, string _lName,
    string _gender, string _blood, string _day, string _month, string _year, address _dad, address _mom)restrigeresul public{
                
        persons[_address].id      = _id;
        persons[_address].blood   = _blood;
        persons[_address].gender  = _gender;
        persons[_address].age     = _age;
        persons[_address].fName   = _fName;
        persons[_address].lName   = _lName;
        persons[_address].day     = _day;
        persons[_address].month   = _month;
        persons[_address].year    = _year;
        persons[_address].dad     = _dad;
        persons[_address].mom     = _mom;

        persons[_dad].son.push(_address);
        
        personAccts.push(_address) -1;
    }
    
    function getPersonAddress() view restrigeresul public returns(address[]) {
        return personAccts;
    }
    
    function getPerson(address _address) view restrigeresul public returns (string, string, string, uint, string, string, string) {
        
        birthdate = persons[_address].day.toSlice().concat("/".toSlice());
        birthdate = birthdate.toSlice().concat(persons[_address].month.toSlice());
        birthdate = birthdate.toSlice().concat("/".toSlice());
        birthdate = birthdate.toSlice().concat(persons[_address].year.toSlice());
        
        return (
            persons[_address].id, 
            persons[_address].fName, 
            persons[_address].lName, 
            persons[_address].age, 
            persons[_address].gender, 
            persons[_address].blood, 
            birthdate);
    }
    
    function getPersonParents(address _address) view restrigeresul public returns(string, string, uint){
        return (
            persons[persons[_address].dad].id, 
            persons[persons[_address].dad].fName, 
            persons[persons[_address].dad].age);
    }

    function getPersonSon(address _address) view restrigeresul public returns(address[]){
        
        return (persons[_address].son);
    }
    
    function countPeople() view restrigeresul public returns (uint) {
        return personAccts.length;
    }
    
    function getPersonHistorial(address _address) view restrigeresul public returns(address[], uint[], uint[]){
        return(
            persons[_address].enfermedades, 
            persons[_address].alergias,
            persons[_address].citasM
            );
    }
    function compareStrings (string a, string b) view public returns (bool){
       return keccak256(a) == keccak256(b);
    }
    
}